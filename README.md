
# Github clone

## Projeto

* O projeto foi criado utilizando React, usando Hooks e boas práticas ES6.


## Instalação

Instalar o projeto e rodar a aplicação.


* Antes de tudo vamos falar de versão do Node. 

Foi utilizado no projeto a versão `v18.17.0` do NodeJs.

Para gerenciar as versões do NodeJs, foi utilizado o `nvm`.


```bash
    nvm use v18.17.0
```

Para rodar o projeto siga o passo-a-passo abaixo:

```bash
  git clone https://gitlab.com/yurimusti/react-github-desbravador.git
  cd react-github-desbravador
  yarn
  yarn start
```


## Considerações finais

- Explicando um pouco a fundo, foi finalizado sem oo drag inn drop dos repositórios, mas eu pesquisei sobre e se tivesse mais tempo para o desenvolvimento, com certeza eu finalizaria.

- Outra explicação é sobre o Redux. Como o projeto não é uma aplicação grande e robusta, não achei válido instalar toda a complexidade do Redux.

- Para os estilos, foi utilizado o `styled-components`. Considerei utilizar o Bootstrap mas como eu tenho mais familiaridade com esses frameworks mais novos, foi utilizado ele. Em relação a responsividade, eu considerei utilizar `styled-system` mas não cheguei a utilizar.


- O Layout é bastante igual ao do Profile do github, sendo feito como `styled-components`.

- O sistema de roteamento foi criado utilizando o `react-router`.

- Foi utilizado o `axios` para conexão com a API do github.

- O sistema foi todo criado com `typescript` seguindo os padrões ES6.

- Outra funcionalidade adicionada foi os `themes` dentro do projeto para podermos utilizar temas `light` ou `dark`

- O Design foi criado bem parecido com o sistema do github onde mostra o usuário default, nesse caso foi o meu usuário no github.

- O sistema não foi hospedado no `heroku` porque minhas contas no heroku estão desabilitadas. Foi utilizado uma hospedagem na `AWS S3`.

```bash
Link: http://desbravador-gihub.s3-website-sa-east-1.amazonaws.com
```
## Melhorias futuras

- Para responsividade mais complexa eu sugiro utilizar uma lib chamada `styled-systems` juntamente com `styled-components` que agiliza muito o tempo de desenvolvimento.

- Implementação de testes unitários e testes de regressão utilizando o `jest`

- Nesse projeto não foi utilizado uma lib de gerenciamento de estados, mas sugiro fortemente uma li mais compacta para esse tipo de projeto utilizando uma lib chamada `rematch`. Para projetos maiores, sugiro utilizar um Redux Saga para gerenciar melhor os estados globais da aplicação.

- No gitlab, onde foi criado o repositório deverá ter uma esteira de deploy criando um `CI/CD` para facilitar o deploy.

- Uma sugestão onde mudamos muito o jeito que o Front-end comunica com o backend eu sugiro implementarmos GraphQL no backend para ser consumida pelo Front-end. Seria uma melhoria muito boa dado que o `GraphQL` vem ganhando muita força nesses ultimos tempos e o tempo de aprendizado dele é bem baixo.
## Autor

- [@yurimusti](https://www.github.com/yurimusti)

